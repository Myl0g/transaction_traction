import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:transaction_traction/classes/Account.dart';

Future<Set<Account>> loadAccounts() async {
  var f =
      File((await getApplicationDocumentsDirectory()).path + '/accounts.json');

  if (!(await f.exists())) return {};

  var j = json.decode(await f.readAsString());
  return (j is List<dynamic> ? j : []).map((e) => Account.fromJson(e)).toSet();
}

Future<void> writeAccounts(Set<Account> accounts) async {
  var f =
      File((await getApplicationDocumentsDirectory()).path + '/accounts.json');

  var j = json.encode(accounts.toList().map((e) => e.toJson()).toList());
  await f.writeAsString(j);
}

// Returns updated set of accounts.
Future<Set<Account>> updateAccount(Account a) async {
  var accounts = await loadAccounts();
  accounts.removeWhere((element) => element.uuid == a.uuid);
  accounts.add(a);
  await writeAccounts(accounts);
  return accounts;
}
