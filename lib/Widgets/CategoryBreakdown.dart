import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:transaction_traction/Widgets/HomePage.dart';
import 'package:transaction_traction/Widgets/TransactionList.dart';
import 'package:transaction_traction/classes/RefreshableWidget.dart';
import 'package:transaction_traction/classes/Transaction.dart';

class CategoryBreakdown extends StatefulWidget {
  CategoryBreakdown({
    Key? key,
    required this.categories,
    required this.parent,
  }) : super(key: key);

  final Map<String, List<Transaction>> categories;
  final GlobalKey<HomePageState> parent;

  @override
  CategoryBreakdownState createState() => CategoryBreakdownState();
}

class CategoryBreakdownState extends RefreshableWidgetState<CategoryBreakdown,
    Map<String, List<Transaction>>> {
  @override
  void refresh(Map<String, List<Transaction>> c) => {};

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Category Breakdown'),
        ),
        body: _content(context),
      );

  Widget _content(BuildContext context) => ListView.separated(
        itemBuilder: _category,
        separatorBuilder: (b, c) => const Divider(),
        itemCount: widget.categories.keys.length,
      );

  ListTile _category(BuildContext context, int index) => ListTile(
        title: Text(widget.categories.keys.toList()[index]),
        subtitle:
            Text(widget.categories[widget.categories.keys.toList()[index]]!
                .fold(
                  0.0,
                  (
                    double previous,
                    Transaction current,
                  ) =>
                      previous + current.amount,
                )
                .toString()),
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => TransactionList(
              title: widget.categories.keys.toList()[index],
              transactions:
                  widget.categories[widget.categories.keys.toList()[index]]!,
              parent: widget.key! as GlobalKey<CategoryBreakdownState>,
            ),
          ),
        ),
      );
}
