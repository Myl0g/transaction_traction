import 'package:flutter/material.dart';
import 'package:transaction_traction/Widgets/TransactionList.dart';
import 'package:transaction_traction/classes/Account.dart';
import 'package:transaction_traction/classes/Transaction.dart';
import 'package:uuid/uuid.dart';

// NOTE: You MUST include either a defaultValue OR account when creating an
// instance of this Widget.
class EditTransactionDialog extends StatefulWidget {
  EditTransactionDialog({
    Key? key,
    required this.parent,
    this.defaultValue,
    this.account,
  }) : super(key: key);

  final GlobalKey<TransactionListState> parent;
  final Transaction? defaultValue;
  final Account? account;

  @override
  EditTransactionDialogState createState() => EditTransactionDialogState();
}

class EditTransactionDialogState extends State<EditTransactionDialog> {
  final descController = TextEditingController();
  final amountController = TextEditingController();
  final merchantController = TextEditingController();
  final dateController = TextEditingController();
  final timeController = TextEditingController();
  final categoriesController = TextEditingController();

  @override
  void dispose() {
    [
      descController,
      amountController,
      merchantController,
      dateController,
      timeController,
      categoriesController,
    ].forEach((element) => element.dispose());
    super.dispose();
  }

  @override
  void initState() {
    descController.text = widget.defaultValue?.description ?? '';
    amountController.text = widget.defaultValue?.amount.toString() ?? '';
    merchantController.text = widget.defaultValue?.merchant ?? '';
    var dateTime = dateToString(widget.defaultValue?.date ?? DateTime.now());
    dateController.text = dateTime[0];
    timeController.text = dateTime[1];
    categoriesController.text =
        categoriesToString(widget.defaultValue?.categories ?? {});

    super.initState();
  }

  // [Date, Time]
  List<String> dateToString(DateTime date) {
    return [
      date.toString().split(' ')[0],
      date.toString().split(' ')[1].split(':').sublist(0, 2).join(':'),
    ];
  }

  String categoriesToString(Map<String, double> categories) {
    var result = '';
    categories
        .forEach((key, value) => result += key + ': ' + value.toString() + ',');
    return result;
  }

  DateTime stringToDate(List<String> str) => DateTime.parse(
        str[0].replaceAll('/', '-') + ' ' + str[1] + ':00',
      );

  Map<String, double> stringToCategories(String str) {
    Map<String, double> categories = {};
    str.split(',').forEach(
          (element) => categories.addAll(
            {
              element.split(':')[0]: double.parse(element.split(':')[1]),
            },
          ),
        );
    return categories;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.defaultValue?.description ?? 'New Transaction'),
      ),
      body: _content(context),
    );
  }

  Widget _textField(
    TextInputType? keyboardType,
    String hintText,
    TextEditingController controller, {
    int? minLines,
    int? maxLines,
  }) =>
      Padding(
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        child: TextField(
          keyboardType: keyboardType,
          decoration: InputDecoration(hintText: hintText),
          controller: controller,
          minLines: minLines,
          maxLines: maxLines,
        ),
      );

  ListView _content(BuildContext context) => ListView(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 12,
              bottom: 16,
              left: 15,
              right: 15,
            ),
            child: TextField(
              decoration: InputDecoration(hintText: 'Description'),
              controller: descController,
            ),
          ),
          _textField(
            TextInputType.number,
            'Amount',
            amountController,
          ),
          _textField(
            null,
            'Merchant',
            merchantController,
          ),
          _textField(
            TextInputType.datetime,
            'Date (YYYY-MM-DD)',
            dateController,
          ),
          _textField(
            TextInputType.datetime,
            'Time (HH:MM)',
            timeController,
          ),
          _textField(
            null,
            'Categories (Name:Amount,Name2:Amount...)',
            categoriesController,
          ),
          Padding(
            padding: EdgeInsets.only(left: 40, right: 40, bottom: 20, top: 20),
            child: ElevatedButton(
              onPressed: () {
                var t = Transaction(
                  uuid: widget.defaultValue?.uuid ?? Uuid().v4(),
                  amount: double.parse(amountController.text),
                  description: descController.text,
                  merchant: merchantController.text,
                  date:
                      stringToDate([dateController.text, timeController.text]),
                  categories: stringToCategories(categoriesController.text),
                  account: widget.defaultValue?.account ?? widget.account!,
                );

                t.account.updateOrAddTransaction(t);
                widget.parent.currentState!
                    .refresh(t.account.transactions.toList());
                Navigator.of(context).pop();
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      );
}
