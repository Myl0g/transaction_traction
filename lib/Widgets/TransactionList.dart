import 'package:flutter/material.dart';
import 'package:transaction_traction/Widgets/EditTransactionDialog.dart';
import 'package:transaction_traction/classes/Account.dart';
import 'package:transaction_traction/classes/RefreshableWidget.dart';
import 'package:transaction_traction/classes/Transaction.dart';

class TransactionList extends StatefulWidget {
  TransactionList({
    Key? key,
    required this.title,
    required this.transactions,
    required this.parent,
    this.account,
  }) : super(key: key);

  final String title;
  final List<Transaction> transactions;
  final GlobalKey<RefreshableWidgetState> parent;
  final Account? account;

  @override
  TransactionListState createState() =>
      TransactionListState(transactions: transactions);
}

class TransactionListState
    extends RefreshableWidgetState<TransactionList, List<Transaction>> {
  TransactionListState({required List<Transaction> transactions})
      : _transactions = transactions;

  List<Transaction> _transactions;

  @override
  void refresh(List<Transaction> t) {
    setState(() => _transactions = t);
    widget.parent.currentState!.setState(() {});
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        floatingActionButton:
            widget.account == null ? null : _addTransactionButton(context),
        body: _content(context),
      );

  Widget _content(BuildContext context) => ListView.separated(
        itemBuilder: _transaction,
        separatorBuilder: (b, i) => const Divider(),
        itemCount: _transactions.length,
      );

  ListTile _transaction(BuildContext context, int index) {
    var transaction = _transactions[index];
    var dateString = transaction.date.day.toString() +
        '/' +
        transaction.date.month.toString() +
        '/' +
        transaction.date.year.toString() +
        ' @ ' +
        transaction.date.hour.toString() +
        ':' +
        (transaction.date.minute < 10
            ? '0' + transaction.date.minute.toString()
            : transaction.date.minute.toString());
    return ListTile(
      title: Text(transaction.description),
      subtitle: Text(
        transaction.amount.toString() +
            '\n' +
            transaction.merchant +
            ', ' +
            dateString,
      ),
      isThreeLine: true,
      onTap: () => showDialog(
        context: context,
        builder: (context) => EditTransactionDialog(
          parent: widget.key! as GlobalKey<TransactionListState>,
          defaultValue: transaction,
        ),
      ),
    );
  }

  FloatingActionButton _addTransactionButton(BuildContext context) =>
      FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => showDialog(
          context: context,
          builder: (context) => EditTransactionDialog(
            parent: widget.key! as GlobalKey<TransactionListState>,
            account: widget.account!,
          ),
        ),
      );
}
