import 'package:flutter/material.dart';
import 'package:transaction_traction/Widgets/HomePage.dart';
import 'package:transaction_traction/storage.dart';
import 'package:transaction_traction/classes/Account.dart';

void main() => runApp(
      TransactionTraction(
        key: GlobalKey(debugLabel: 'Whole app'),
      ),
    );

class TransactionTraction extends StatelessWidget {
  TransactionTraction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        key: GlobalKey(debugLabel: 'Material app'),
        title: 'Transaction Traction',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          body: _content(context),
        ),
      );

  Widget _content(BuildContext context) => FutureBuilder<List<Account>>(
        future: loadAccounts().then((s) => s.toList()),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return HomePage(
              key: GlobalKey<HomePageState>(debugLabel: 'Home page'),
              accounts: snapshot.data!,
              parent: key as GlobalKey,
            );
          } else {
            return Align(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
        },
      );
}
