// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) {
  return Transaction(
    uuid: json['uuid'] as String,
    amount: (json['amount'] as num).toDouble(),
    description: json['description'] as String,
    merchant: json['merchant'] as String,
    date: DateTime.parse(json['date'] as String),
    categories: Map<String, double>.from(json['categories'] as Map),
    account: Account.fromJson(json['account'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'amount': instance.amount,
      'description': instance.description,
      'merchant': instance.merchant,
      'date': instance.date.toIso8601String(),
      'categories': instance.categories,
      'account': instance.account.toJson(),
    };
