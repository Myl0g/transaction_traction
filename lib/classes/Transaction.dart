import 'package:json_annotation/json_annotation.dart';
import 'package:transaction_traction/classes/Account.dart';

part 'Transaction.g.dart';

@JsonSerializable(explicitToJson: true)
class Transaction {
  Transaction({
    required this.uuid,
    required this.amount,
    required this.description,
    required this.merchant,
    required this.date,
    required this.categories,
    required this.account,
  });

  String uuid;
  double amount;
  String description;
  String merchant;
  DateTime date;
  Map<String, double> categories;
  Account account;

  factory Transaction.fromJson(Map<String, dynamic> json) =>
      _$TransactionFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionToJson(this);
}
